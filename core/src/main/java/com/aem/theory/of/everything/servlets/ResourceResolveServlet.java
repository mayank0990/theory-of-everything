package com.aem.theory.of.everything.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(
		immediate = true,
		service = { Servlet.class },
		configurationPid = "com.aem.theory.of.everything.servlets.ResourceResolveServlet",
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Resource Resolution Servlet",
				"sling.servlet.paths=" + "/bin/resourceResolve",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET
		})
public class ResourceResolveServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(ResourceResolveServlet.class);

	private ResourceResolver resourceResolver;
	private String res = "/content/demo";
	private String demoResPath = "";
	private String compName = "";

	@Activate
	@Modified
	protected void activate() {
		log.info("ResourceResolveServlet activated!");
	}

	/**In Progress*/
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {	
		PrintWriter out = response.getWriter();
		//out.println("Resource Resolution Servlet running...\n");
		response.setContentType("text/html");

		try {
			this.resourceResolver = request.getResourceResolver();
			Resource demoRes = resourceResolver.getResource(res + "/jcr:content/section-par/demo");

			if(demoRes != null && demoRes instanceof Resource) {
				ValueMap demoProps = demoRes.getValueMap();
				demoResPath = demoProps.get("sling:resourceType", String.class);
				//out.println(demoResPath);
				//out.println(demoRes.getName());
				out.println(demoRes.getName() + ".html");
			}
			
		} catch(Exception e) {
			out.println("Exception in doGet() " + e.getMessage());
		}	
	}

}
