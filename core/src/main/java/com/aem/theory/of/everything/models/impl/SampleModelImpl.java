package com.aem.theory.of.everything.models.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.theory.of.everything.models.SampleModel;
import com.aem.theory.of.everything.objects.MenuItem;
import com.google.gson.Gson;

/**
 * Sling Model Implementation of Sample Model
 *
 * @author Mayank
 *
 */
@Model(adaptables=Resource.class, adapters=SampleModel.class, defaultInjectionStrategy=DefaultInjectionStrategy.OPTIONAL)
public class SampleModelImpl implements SampleModel {
	
	/**Static Logger*/
	private static Logger log = LoggerFactory.getLogger(SampleModel.class);
	
	@Inject
	@Default(values="")
	private String firstName;
	
	@Inject
	@Named("jcr:createdBy")		// To define Alias or Nick Name for a property
	private String author;
	
	@Inject
	@Default(values="{}")
	private String[] listofItems;
	
	private List<MenuItem> listofLinks;
	
	private String authorName;
	
	@PostConstruct
	public void init() {
		log.info("Sample Model init Method!");
		
		firstName = firstName.toUpperCase();
		authorName = author;
		
		Gson gson = new Gson();
		listofLinks = new ArrayList<>();
		
		for(String eachString : listofItems) {
			MenuItem eachItem = gson.fromJson(eachString, MenuItem.class);
			listofLinks.add(eachItem);
		}
	}
	
	
	@Override
	public String getAuthorName() {
		return authorName;
	}

	@Override
	public String getName() {
		return firstName;
	}

	@Override
	public List<MenuItem> getListofLinks() {
		return listofLinks;
	}
	
}
