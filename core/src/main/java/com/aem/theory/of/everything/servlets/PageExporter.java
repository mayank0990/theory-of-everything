package com.aem.theory.of.everything.servlets;

import static org.apache.sling.query.SlingQuery.$;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.query.SlingQuery;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mayank
 * Page JSON Exporter Servlet
 *
 */
@Component(
		immediate = true,
		service= {Servlet.class},
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Page Exporter Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths="+ "/bin/fetchChild"
		})
public class PageExporter extends SlingAllMethodsServlet {
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(PageExporter.class);

	private ResourceResolver resourceResolver;

	private Resource resource;

	@Activate
	protected void activate() {
		log.info("PageExporter activated!");

	}
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {	
		PrintWriter out = response.getWriter();
		out.println("Page JSON Exporter Servlet running...\n");
		
		resourceResolver = request.getResourceResolver();
		resource = resourceResolver.getResource("/content/royal-enfield/in/en");

		try {
			SlingQuery slingQuery = $(resource).children();
				for(Resource res : slingQuery) {
					if(!res.getPath().matches("jcr:content")) {
						out.println("Inside JCR");
						SlingQuery sling = $(resource).children();
						for(Resource resrc : sling) {
							out.println(resrc.getPath());
						}
					}
			}
		} catch(Exception e) {
			log.error("Exception in Page JSON Exporter Servlet :: ",e);
		}
	}


}
