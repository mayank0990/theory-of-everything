package com.aem.theory.of.everything.objects;

/**DAO*/
public class MenuItem {
	
	private String text;
	private String description;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
