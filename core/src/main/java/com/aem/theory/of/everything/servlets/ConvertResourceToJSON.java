package com.aem.theory.of.everything.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonObject;


@Component(
		immediate = true,
		service= {Servlet.class},
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Convert Resource to JSON Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths="+ "/bin/convertResource",
		})
public class ConvertResourceToJSON extends SlingAllMethodsServlet {
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ConvertResourceToJSON.class);

	private ResourceResolver resourceResolver;

	private Resource resource;
	
	Session session;

	@Activate
	protected void activate() {
		log.info("SlingQueryServlet activated!");

	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {	
		PrintWriter out = response.getWriter();
		//out.println("Convert Resource to JSON Servlet....");
		
		try {
			resourceResolver = request.getResourceResolver();
			session = resourceResolver.adaptTo(Session.class);
			
			List<String> pathList = new ArrayList<>();
			
			Node rootNode = session.getRootNode();
			Node contentNode = rootNode.getNode("content/royal-enfield");
			NodeIterator itr = contentNode.getNodes();
			while(itr.hasNext()) {
				Node cNode = itr.nextNode();
				pathList.add(cNode.getPath());
				if(cNode.hasNodes()) {
					itr = cNode.getNodes();
				} else {
					continue;
				}
			}
			
			List<String> copyCat = new ArrayList<>();
			for(String str : pathList) {
				if(str.contains("header/")) {
					copyCat.add(str);
				}
			}
			
			for(String str : copyCat) {
				//out.println(str);
				Resource resource = resourceResolver.getResource(str);
				ValueMap props = resource.getValueMap();
				String slingRes = "/apps/"+props.get("sling:resourceType","");
				out.println(slingRes);
				
			}
			
		} catch(Exception e) {
			out.println("Exception in Convert Resource to JSON :: " + e.getMessage());
		}
	
	}
	
	 protected final JsonObject serializeToJSON(final Resource resourceToSerialize)
	            throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {

	        final DateTimeFormatter dtf = ISODateTimeFormat.dateTime();
	        final Map<String, Object> serializedData = new HashMap<String, Object>();

	        for (Map.Entry<String, Object> entry : resourceToSerialize.getValueMap().entrySet()) {
	            if (entry.getValue() instanceof Calendar) {
	                final Calendar cal = (Calendar) entry.getValue();
	                serializedData.put(entry.getKey(), dtf.print(cal.getTimeInMillis()));
	            } else if (entry.getValue() instanceof Date) {
	                final Date date = (Date) entry.getValue();
	                serializedData.put(entry.getKey(), dtf.print(date.getTime()));
	            } else {
	                serializedData.put(entry.getKey(), entry.getValue());
	            }
	        }

	        Gson gson = new Gson();
	        return gson.toJsonTree(serializedData).getAsJsonObject();
	    }
}
