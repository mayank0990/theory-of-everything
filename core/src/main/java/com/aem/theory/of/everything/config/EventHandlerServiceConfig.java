package com.aem.theory.of.everything.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name="Event Handler Service Config", description="Configure Event Handler Service")
public @interface EventHandlerServiceConfig {
	
	@AttributeDefinition(name="Listener Path", description="The path on which event listener listens")
	String getPath() default "/content";

}
