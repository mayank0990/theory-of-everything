package com.aem.theory.of.everything.models;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;

import com.aem.theory.of.everything.objects.MenuItem;

@ConsumerType
public interface SampleModel {
	
	public String getName();
	
	public String getAuthorName();
	
	public List<MenuItem> getListofLinks();

}
