package com.aem.theory.of.everything.services.impl;

import java.io.IOException;
import java.io.PrintWriter;

import javax.jcr.RepositoryException;
import javax.jcr.SimpleCredentials;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.auth.core.spi.AuthenticationFeedbackHandler;
import org.apache.sling.auth.core.spi.AuthenticationHandler;
import org.apache.sling.auth.core.spi.AuthenticationInfo;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.crx.security.token.TokenUtil;

/**
 * Custom Authentication Handler 
 *
 * @author Mayank & Akash
 *
 */
@Component(
		immediate = true,
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Custom Authentication Handler",
				AuthenticationHandler.PATH_PROPERTY + "=" + "/customAuthHandler"

		})
public class CustomAuthenticationHandler implements AuthenticationHandler, AuthenticationFeedbackHandler {

	/**Static Logger*/
	private static final Logger log = LoggerFactory.getLogger(CustomAuthenticationHandler.class);
	
	@Reference
	private SlingRepository slingRepository;

	@Activate
	public void activate() {
		log.info("CustomAuthenticationHandler activated!");
	}

	@Override
	public void authenticationFailed(HttpServletRequest req, HttpServletResponse res, AuthenticationInfo arg2) {
		try {
			PrintWriter out = res.getWriter();
			res.setStatus(HttpServletResponse.SC_OK);
			res.setContentType("text/plain");
			out.println("Authentication Failed");
			res.flushBuffer();
		} catch (IOException e) {
			log.info("Exception in Auth Failed ",e);
		}

	}

	@Override
	public boolean authenticationSucceeded(HttpServletRequest req, HttpServletResponse res, AuthenticationInfo arg2) {
		try {
			PrintWriter out = res.getWriter();
			String username = req.getParameter("username");
			TokenUtil.createCredentials(req, res, slingRepository,
					username, true); 	// It will throw Repository Exception if we'll not Whitelist the bundle
			res.setStatus(HttpServletResponse.SC_OK);
			res.setContentType("text/plain");
			out.println("Authentication Succeedded");
			res.flushBuffer();
		} catch (IOException | RepositoryException e) {
			log.info("Exception in Auth Succeeded ",e);
		}

		return true;
	}

	@Override
	public void dropCredentials(HttpServletRequest arg0, HttpServletResponse arg1) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public AuthenticationInfo extractCredentials(HttpServletRequest request, HttpServletResponse response) {
		String user = request.getParameter("username");
		String pass = request.getParameter("pass");
		AuthenticationInfo info = null;

		try {
			SimpleCredentials credentials = new SimpleCredentials(user, pass.toCharArray());
			info = new AuthenticationInfo(HttpServletRequest.FORM_AUTH, credentials.getUserID());
			info.put(JcrResourceConstants.AUTHENTICATION_INFO_CREDENTIALS, credentials);
		} catch(Exception e) {
			log.error("Exception in Extract Credentials :: ",e);
		}

		return info;
	}

	@Override
	public boolean requestCredentials(HttpServletRequest arg0, HttpServletResponse arg1) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

}
