package com.aem.theory.of.everything.models.templates;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Base Page Sling Model 
 *
 * @author Akash
 *
 */
@ConsumerType
public interface BasePageModel {
	
	public String getTitle();
	
}
