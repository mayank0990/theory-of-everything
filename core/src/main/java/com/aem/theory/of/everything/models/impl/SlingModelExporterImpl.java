package com.aem.theory.of.everything.models.impl;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.aem.theory.of.everything.models.SlingModelExporter;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Sling Model Exporter
 *
 * @author Mayank
 *
 */
@Model(
		adaptables=Resource.class, adapters=SlingModelExporter.class, 
		resourceType = "theory-of-everything/components/content/slingModelExporter",
		defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)

@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, selector = "model",
extensions = ExporterConstants.SLING_MODEL_EXTENSION)

@JsonSerialize(as = SlingModelExporter.class)
public class SlingModelExporterImpl implements SlingModelExporter {

	/**Static Logger*/
	private static Logger log = LoggerFactory.getLogger(SlingModelExporterImpl.class);

	//static final String RESOURCE_TYPE = "theory-of-everything/components/content/slingModelExporter";

	@Inject
	@Default(values="")
	private String firstName;

	@Inject
	@Default(values="")
	private String lastName;

	@Inject
	@Default(values="")
	private String address;

	@Inject
	@Default(values="")
	private String phone;

	@PostConstruct
	protected void initModel() {

		firstName = firstName.toUpperCase();
		lastName = lastName.toUpperCase();
		address = address.toUpperCase();
		phone = "+91-"+phone;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	@Override
	public String getAddress() {
		return address;
	}

	@Override
	public String getPhone() {
		return phone;
	}

	/*@Override
	public String getExportedType() {
		return RESOURCE_TYPE;
	}*/
}
