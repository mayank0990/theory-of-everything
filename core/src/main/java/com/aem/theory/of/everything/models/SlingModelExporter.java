package com.aem.theory.of.everything.models;

import org.osgi.annotation.versioning.ConsumerType;

import com.fasterxml.jackson.annotation.JsonProperty;

@ConsumerType
public interface SlingModelExporter {
	
	@JsonProperty("firstName")
	public String getFirstName();
	
	@JsonProperty("lastName")
	public String getLastName();
	
	@JsonProperty("address")
	public String getAddress();
	
	@JsonProperty("phone")
	public String getPhone();

}
