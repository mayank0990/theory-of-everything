package com.aem.theory.of.everything.servlets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mayank
 * SOAP API Servlet
 *
 */
@Component(
		immediate = true,
		service= {Servlet.class},
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "SOAP API Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths="+ "/bin/soapRes"
		})
public class SOAPServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(SOAPServlet.class);

	@Activate
	protected void activate() {
		log.info("SlingQueryServlet activated!");

	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {	
		PrintWriter out = response.getWriter();
		out.println("SOAP API Servlet running...\n");
		
		soapRequest();

	}

	public static void soapRequest() {
		try {

			final String END_POINT_URL = "http://www.hyundaiauto.in:8888/eai_tst_anon_enu/start.swe?SWEExtSource=SecureWebService&SWEExtCmd=Execute";
			final String PREFERRED_PREFIX = "soapenv";
			final String DATA_PREFIX = "data";
			
			String uri = "http://siebel.com/CustomUI";
			String dataUri = "http://www.siebel.com/xml/HMI_EAI_Inbound_Lead/Data";
			
			/**Data fields*/
			String user = "CRMIFUSER";
			String pass = "Gcrm#2014";
			String areaCode = "ES07";
			String cityCode = "522";
			String carModel = "Swift Dzire";
			String userEmail = "guptanaman@gmail.com";
			String userMobile = "9971334055";
			String userName = "NAMAN GUPTA";
			
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapMessage = messageFactory.createMessage();
			
			SOAPEnvelope envelope = soapMessage.getSOAPPart().getEnvelope();
			SOAPHeader soapHeader = soapMessage.getSOAPHeader();
			SOAPElement wsseSecurity = soapHeader.addChildElement("Security", "wsse", "http://schemas.xmlsoap.org/ws/2003/06/secext");
			SOAPElement usernameToken = wsseSecurity.addChildElement("UsernameToken", "wsse");
			usernameToken.setAttribute("wsu:Id", "sample");
			usernameToken.setAttribute("xmlns:wsu", "http://schemas.xmlsoap.org/ws/2003/06/utility");

			SOAPElement username = usernameToken.addChildElement("Username","wsse");
			username.addTextNode(user);

			SOAPElement password = usernameToken.addChildElement("Password", "wsse");
			password.setAttribute("Type", "wsse:PasswordText");
			password.addTextNode(pass);

			envelope.removeNamespaceDeclaration(envelope.getPrefix());
			envelope.setPrefix(PREFERRED_PREFIX);
			envelope.addNamespaceDeclaration("cus", uri);
			envelope.addNamespaceDeclaration("data", dataUri);
			soapHeader.setPrefix(PREFERRED_PREFIX);
			SOAPBody soapBody = soapMessage.getSOAPBody();
			soapBody.setPrefix(PREFERRED_PREFIX);

			SOAPElement cusHMINode = soapBody.addChildElement("HMI_spcEAI_spcInbound_spcLeadInsert_Input","cus");
			SOAPElement listofHmiNode = cusHMINode.addChildElement("ListOfHmi_Eai_Inbound_Lead", DATA_PREFIX);
			listofHmiNode.setAttribute("lastpage", "?");
			listofHmiNode.setAttribute("recordcount", "?");

			SOAPElement hmiEaiNode = listofHmiNode.addChildElement("HmiEaiInboundLead", DATA_PREFIX);
			hmiEaiNode.setAttribute("Operation", "?");

			SOAPElement addr = hmiEaiNode.addChildElement("ADDR", DATA_PREFIX);

			SOAPElement area = hmiEaiNode.addChildElement("AREA", DATA_PREFIX);
			area.addTextNode(areaCode);

			SOAPElement campCode = hmiEaiNode.addChildElement("CAMP_CODE", DATA_PREFIX);

			SOAPElement campConId = hmiEaiNode.addChildElement("CAMP_CON_ID", DATA_PREFIX);

			SOAPElement city = hmiEaiNode.addChildElement("CITY", DATA_PREFIX);
			city.addTextNode(cityCode);

			SOAPElement description = hmiEaiNode.addChildElement("DESCRIPTION", DATA_PREFIX);
			description.addTextNode("Customer wants to test drive of Elite i10");

			SOAPElement corpName = hmiEaiNode.addChildElement("CORP_NAME", DATA_PREFIX);

			SOAPElement curAccessories = hmiEaiNode.addChildElement("CUR_ACCESSORIES", DATA_PREFIX);

			SOAPElement curMileage = hmiEaiNode.addChildElement("CUR_MILEAGE", DATA_PREFIX);

			SOAPElement curModel = hmiEaiNode.addChildElement("CUR_MODEL", DATA_PREFIX);
			curModel.addTextNode(carModel);

			SOAPElement regNum = hmiEaiNode.addChildElement("REG_NUM", DATA_PREFIX);

			SOAPElement curSellPrice = hmiEaiNode.addChildElement("CUR_SELL_PRICE", DATA_PREFIX);

			SOAPElement curVin = hmiEaiNode.addChildElement("CUR_VIN", DATA_PREFIX);

			SOAPElement curVariant = hmiEaiNode.addChildElement("CUR_VARIANT", DATA_PREFIX);

			SOAPElement curMfg = hmiEaiNode.addChildElement("CUR_MFG_OF_CAR", DATA_PREFIX);

			SOAPElement dlrCd = hmiEaiNode.addChildElement("DLR_CD", DATA_PREFIX);

			SOAPElement designation = hmiEaiNode.addChildElement("DESIGNATION", DATA_PREFIX);

			SOAPElement email = hmiEaiNode.addChildElement("EMAIL", DATA_PREFIX);
			email.addTextNode(userEmail);

			SOAPElement landlineCd = hmiEaiNode.addChildElement("LANDLINE_CD", DATA_PREFIX);

			SOAPElement landlineNum = hmiEaiNode.addChildElement("LANDLINE_NUM", DATA_PREFIX);

			SOAPElement leadReqDate = hmiEaiNode.addChildElement("LEAD_REQ_DATE", DATA_PREFIX);
			leadReqDate.addTextNode("12122018 000000");

			SOAPElement leadKey = hmiEaiNode.addChildElement("LEAD_KEY", DATA_PREFIX);
			leadKey.addTextNode("54654564");

			SOAPElement mobile = hmiEaiNode.addChildElement("MOBILE", DATA_PREFIX);
			mobile.addTextNode(userMobile);

			SOAPElement name = hmiEaiNode.addChildElement("NAME", DATA_PREFIX);
			name.addTextNode(userName);

			SOAPElement numOfCars = hmiEaiNode.addChildElement("NUM_OF_CARS", DATA_PREFIX);
			numOfCars.addTextNode("?");

			SOAPElement salutation = hmiEaiNode.addChildElement("SALUTATION", DATA_PREFIX);
			salutation.addTextNode("Mr.");

			SOAPElement sourceCd = hmiEaiNode.addChildElement("SOURCE_CD", DATA_PREFIX);
			sourceCd.addTextNode("2510");

			SOAPElement state = hmiEaiNode.addChildElement("STATE", DATA_PREFIX);
			state.addTextNode("UP");

			SOAPElement subArea = hmiEaiNode.addChildElement("SUB_AREA", DATA_PREFIX);

			SOAPElement reqTestDrive = hmiEaiNode.addChildElement("REQ_TEST_DRV", DATA_PREFIX);
			reqTestDrive.addTextNode("YES");

			SOAPElement type = hmiEaiNode.addChildElement("TYPE", DATA_PREFIX);
			type.addTextNode("ES");

			SOAPElement corpType = hmiEaiNode.addChildElement("CORP_TYPE", DATA_PREFIX);

			SOAPElement vehicleofInterest = hmiEaiNode.addChildElement("VEHICLE_OF_INTEREST", DATA_PREFIX);
			vehicleofInterest.addTextNode("Grand i10");

			SOAPElement voiMileage = hmiEaiNode.addChildElement("VOI_MILEAGE", DATA_PREFIX);

			SOAPElement voiSellPrice = hmiEaiNode.addChildElement("VOI_SELL_PRICE", DATA_PREFIX);

			SOAPElement voiMfgCar = hmiEaiNode.addChildElement("VOI_MFG_OF_CAR", DATA_PREFIX);

			SOAPElement voiVariant = hmiEaiNode.addChildElement("VOI_VARIANT", DATA_PREFIX);

			SOAPElement vehicleofInterest2 = hmiEaiNode.addChildElement("VEHICLE_OF_INTEREST_2", DATA_PREFIX);

			SOAPElement vehicleofInterest3 = hmiEaiNode.addChildElement("VEHICLE_OF_INTEREST_3", DATA_PREFIX);

			SOAPElement voiFiFLag = hmiEaiNode.addChildElement("VOI_FI_FLG", DATA_PREFIX);

			SOAPElement eaiRtcd = hmiEaiNode.addChildElement("EAI_RTCD", DATA_PREFIX);

			SOAPElement voiFuelDesc = hmiEaiNode.addChildElement("VOI_FUEL_DESC", DATA_PREFIX);

			SOAPElement eaiMsg = hmiEaiNode.addChildElement("EAI_MSG", DATA_PREFIX);

			SOAPElement voiMake = hmiEaiNode.addChildElement("VOI_MAKE", DATA_PREFIX);

			SOAPElement curColor = hmiEaiNode.addChildElement("CUR_COLOR", DATA_PREFIX);

			SOAPElement curFuelDesc = hmiEaiNode.addChildElement("CUR_FUEL_DESC", DATA_PREFIX);

			SOAPElement curNumOwn = hmiEaiNode.addChildElement("CUR_NUM_OF_OWN", DATA_PREFIX);

			SOAPElement curMake = hmiEaiNode.addChildElement("CUR_MAKE", DATA_PREFIX);

			SOAPElement preDriveDate = hmiEaiNode.addChildElement("PRE_DRIVE_DATE", DATA_PREFIX);

			SOAPElement preTimeSlot = hmiEaiNode.addChildElement("PRE_TIME_SLOT", DATA_PREFIX);

			soapMessage.saveChanges();

			/*Creating Connection*/
			SOAPConnectionFactory soapConFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection connection = soapConFactory.createConnection();
			URL url = new URL(END_POINT_URL);
			SOAPMessage soapResponse = connection.call(soapMessage, url);

			//Print XML Request Message
			String request = "";
			ByteArrayOutputStream requestXML = new ByteArrayOutputStream();
			soapMessage.writeTo(requestXML);
			request = requestXML.toString();
			/*out.println("SOAP REQUEST XML");
        	out.println(result);
        	out.flush();*/


			//Print XML Response Message
			String response = "";
			ByteArrayOutputStream responseXml = new ByteArrayOutputStream();
			soapResponse.writeTo(responseXml);
			response = responseXml.toString();
			/*out.println("\n\nSOAP RESPONSE XML");
        	out.println(res);
        	out.flush();*/

		} catch(Exception e) {
			e.printStackTrace();
		}
	}


}
