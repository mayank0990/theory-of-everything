package com.aem.theory.of.everything.services.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.theory.of.everything.config.EventListenerServiceConfig;
import com.aem.theory.of.everything.services.EventListenerService;

/**
 * Event Listener Service
 *
 * @author Mayank
 *
 */
@Component(
		immediate = true,
		service = { EventListenerService.class },
		enabled = true,
		configurationPolicy = ConfigurationPolicy.REQUIRE,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Event Listener Service"
		})
@Designate(ocd=EventListenerServiceConfig.class)
public class EventListenerServiceImpl implements EventListenerService, EventListener {

	/**Static Logger*/
	private static Logger log = LoggerFactory.getLogger(EventListenerServiceImpl.class);

	private String path;

	@Reference
	ResourceResolverFactory resourceResolverFactory;

	private ResourceResolver resourceResolver;
	private Session session;
	private ObservationManager observationManager;

	@Activate
	public void activate(EventListenerServiceConfig config) {
		log.info("Activated EventListenerServiceImpl!");

		this.path = config.getPath();

		Map<String,Object> params = new HashMap<>();
		params.put(ResourceResolverFactory.SUBSERVICE, "listenEvent");
		try {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(params);
			session = resourceResolver.adaptTo(Session.class);
			log.info("User ID :: " + session.getUserID());

			// Setup the event handler to respond to a new claim under specific path.... 
			observationManager = session.getWorkspace().getObservationManager();
			observationManager.addEventListener(this, Event.NODE_ADDED, this.path, true, null, null, false);
		} catch (LoginException e) {
			log.error("LoginExcepion in activate method of EventListenerServiceImpl :: ", e);
		} catch (Exception e) {
			log.error("Exception in activate method of EventListenerServiceImpl :: ", e);
		}
	}

	@Override
	public void onEvent(EventIterator it) {
		log.info("IN ONEVENT");
		try {
			while(it.hasNext()) {
				Event event = it.nextEvent();
				log.info("New Event Added - " + event.getPath());
				
				Node pageContentNode = session.getNode(event.getPath());
				pageContentNode.setProperty("listened", "Event Listened");
				session.save();
			}
		} catch(Exception e) {
			log.error("Exception in onEvent Method :: " , e);
		}
	}

	@Deactivate
	protected void deactivate() throws RepositoryException {
		if(observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}


}
