package com.aem.theory.of.everything.services.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.theory.of.everything.config.EventHandlerServiceConfig;
import com.day.cq.replication.ReplicationAction;

/**
 * Event Handler Service
 *
 * @author Mayank
 *
 */
@Component(
		immediate = true,
		service = { EventHandler.class },
		enabled = true,
		configurationPolicy = ConfigurationPolicy.REQUIRE,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Event Handler Service",
				"event.topics=" + ReplicationAction.EVENT_TOPIC
		})
@Designate(ocd=EventHandlerServiceConfig.class)
public class EventHandlerService implements EventHandler {

	/**Static Logger*/
	private static Logger log = LoggerFactory.getLogger(EventHandlerService.class);

	private String path;

	@Reference
	ResourceResolverFactory resourceResolverFactory;

	private ResourceResolver resourceResolver;
	private Session session;

	@Activate
	public void activate(EventHandlerServiceConfig config) {
		log.info("Activated EventHandlerService!");

		this.path = config.getPath();

		Map<String,Object> params = new HashMap<>();
		params.put(ResourceResolverFactory.SUBSERVICE, "handleEvent");
		try {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(params);
			session = resourceResolver.adaptTo(Session.class);
			log.info("User ID :: " + session.getUserID());
		} catch (LoginException e) {
			log.error("LoginExcepion in activate method of EventHandlerService :: ", e);
		} catch (Exception e) {
			log.error("Exception in activate method of EventHandlerService :: ", e);
		}
	}

	@Override
	public void handleEvent(Event event) {
		ReplicationAction action = ReplicationAction.fromEvent(event);
		if(action != null) {
			log.info("Replication action {} occured on {} ", action.getType().getName(), action.getPath());
		}
		log.info("");
	}

}
