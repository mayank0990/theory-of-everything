package com.aem.theory.of.everything.servlets;

import static org.apache.sling.query.SlingQuery.$;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.query.SlingQuery;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Akash
 * Sling Query Servlet
 *
 */
@Component(
		immediate = true,
		service= {Servlet.class},
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Sling Query Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths="+ "/bin/slingQuery",
				"sling.servlet.resourceTypes=" + "theory-of-everything/components/page/basePage",
				"sling.servlet.extensions=" + "everything"
		})
public class SlingQueryServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(SlingQueryServlet.class);

	private ResourceResolver resourceResolver;

	private Resource resource;

	@Activate
	protected void activate() {
		log.info("SlingQueryServlet activated!");

	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {	
		PrintWriter out = response.getWriter();
		out.println("Sling Query Servlet running...\n");
		resourceResolver = request.getResourceResolver();
		resource = resourceResolver.getResource("/content/we-retail");

		try {
			SlingQuery slingQuery = $(resource).children("cq:Page");
			for(Resource res : slingQuery) {
				out.println(res.getPath());
			}
		} catch(Exception e) {
			log.error("Exception in Sling Query Servlet :: ",e);
		}
	}


}
