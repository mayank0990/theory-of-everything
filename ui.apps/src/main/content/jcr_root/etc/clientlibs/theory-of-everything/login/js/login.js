document.addEventListener("keydown", function(event) {
	if(event.which == 13) {
		$('.customLoginBtn').click(function() {
			var data = $('.loginForm').serialize();
			let params = new URLSearchParams(document.location.search.substring(1));
			$.ajax({
				url: '/customAuthHandler',
				type: "POST",
				data: data,
				success: function(res) {
					window.location.assign(params.get('resource'));
					console.log(res);
				},
				error: function(e) {
					console.log(e);
				}
			});
		});
	}
});
